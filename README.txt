To build the app: ./gradlew clean build
To run the app: ./gradlew bootRun
Documentation of REST API available at: http://localhost:8200/swagger-ui/

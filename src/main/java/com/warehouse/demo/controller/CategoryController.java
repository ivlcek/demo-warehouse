package com.warehouse.demo.controller;

import java.util.List;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.warehouse.demo.dto.CategoryDTO;
import com.warehouse.demo.model.Category;
import com.warehouse.demo.service.CategoryService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;


@RestController
@CrossOrigin
@RequestMapping(value = "/category")
@Api(tags = { "CategoryController" })
@SwaggerDefinition(tags = { @Tag(name = "CategoryController", description = "Category Controller provides basic CRUD reactive rest api methods") })
public class CategoryController {

	private final CategoryService categoryService;

	public CategoryController(CategoryService categoryService) {
		this.categoryService = categoryService;
	}
	
	@GetMapping("/findByName/{name}")
	@ApiOperation(value = "Find category by name")
	public Mono<Category> findByName(@PathVariable String name) {
		return categoryService.findByName(name);
	}
		
	@GetMapping("/findAll")
	@ApiOperation(value = "Find all categories")
	public Flux<Category> findAll() {
		return categoryService.findAll();
	}

	@PutMapping("/addCategory")
	@ApiOperation(value = "Add new Category and attach it as a child to parent Category")
	public Mono<Category> addCategory(
			@ApiParam(value = "Category to be added to parent Category") @Validated @RequestBody Category category) {
		return categoryService.addCategory(category, category.getParent());
	}
	
	@DeleteMapping("/deleteByName/{name}")
	@ApiOperation(value = "Delete Category and all its children for given category name")
	public Mono<Void> deleteByName(@PathVariable String name) {
		return categoryService.removeCategory(name);
	}

	@GetMapping("/findByNameContainingIgnoreCase")
	@ApiOperation(value = "Find Categories containing name while ignoring case")	
	public Flux<Category> findByNameContainingIgnoreCase(@RequestParam String name) {			
		return categoryService.findByNameContainingIgnoreCase(name);
	}
	
	@GetMapping("/getTree/{name}")
	@ApiOperation(value = "Get a tree of categories for given category name")
	public Mono<List<CategoryDTO>> getTree(@PathVariable String name) {
		return categoryService.getTree(name);
	}
}

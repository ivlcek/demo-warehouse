package com.warehouse.demo.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.data.domain.Sort.Direction;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.warehouse.demo.model.Product;
import com.warehouse.demo.service.ProductService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.SwaggerDefinition;
import io.swagger.annotations.Tag;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@CrossOrigin
@RequestMapping(value = "/product")
@Api(tags = { "ProductController" })
@SwaggerDefinition(tags = { @Tag(name = "ProductController", description = "Product Controller provides basic CRUD reactive rest api methods") })
public class ProductController {

	private final ProductService productService;

	public ProductController(ProductService productService) {
		this.productService = productService;
	}
	
	@GetMapping("/findAll")
	@ApiOperation(value = "Find all products")
	public Flux<Product> findAll() {
		return productService.findAll();
	}		
		
	@GetMapping("/findById/{id}")
	@ApiOperation(value = "Find product by id")
	public Mono<Product> findById(@PathVariable String id) {
		return productService.findById(id);
	}
	
	/**
	 * Delete product by given Id.
	 * 
	 * @param id of product to be deleted
	 * @return Mono<Void>
	 */	
	@DeleteMapping("/deleteById/{id}")
	@ApiOperation(value = "Delete product by id")
	public Mono<Void> deleteById(@PathVariable String id) {
		return productService.deleteByIds(Arrays.asList(id));
	}
	
	/**
	 * Delete products by given Ids.
	 * 
	 * @param ids of products to be deleted
	 * @return Mono<Void>
	 */	
	@DeleteMapping("/deleteByIds")
	@ApiOperation(value = "Delete Products by given product ids")
	public Mono<Void> deleteByIds(
			@ApiParam(value = "Products to be deleted") @RequestBody List<String> ids) {
		return productService.deleteByIds(ids);
	}		
	
	@PutMapping("/save")
	@ApiOperation(value = "Save new product or update existing product")
	public Mono<Product> save(
			@ApiParam(value = "Product to be saved") @RequestBody @Validated Product product) {
		return productService.save(product);
	}
	
	@PostMapping("/findByExamplePageable")
	@ApiOperation(value = "Find all products by provided product example with pageable feature. "
			+ "If emply product then all products will be retrieved.")
	public Flux<Product> findByExamplePageable(
			@ApiParam(value = "Product example to be used for search") @RequestBody Product product,
			final @RequestParam(name = "page") int page,
			final @RequestParam(name = "size") int size,
			final @RequestParam(name = "sortBy") String sortBy,
			final @RequestParam(name = "direction") Direction direction) {
		return productService.findByExamplePageable(product, page, size, sortBy, direction);
	}
	
	@PostMapping("/findByExamplePageableCount")
	@ApiOperation(value = "Find count of all products by provided product example with pageable feature. "
			+ "If emply Product then all products will be retrieved.")
	public Mono<Long> findByExamplePageableCount(
			@ApiParam(value = "Product example to be used for search") @RequestBody Product product,
			final @RequestParam(name = "page") int page,
			final @RequestParam(name = "size") int size,
			final @RequestParam(name = "sortBy") String sortBy,
			final @RequestParam(name = "direction") Direction direction) {
		return productService.findByExamplePageableCount(product, page, size, sortBy, direction);
	}
	
	@PostMapping("/findByCategories")
	@ApiOperation(value = "Find all products for given category ids")
	public Flux<Product> findByCategories(
			@ApiParam(value = "Collection of category Ids") @RequestBody List<String> categoryIds,
			final @RequestParam(name = "page") int page,
			final @RequestParam(name = "size") int size,
			final @RequestParam(name = "sortBy") String sortBy,
			final @RequestParam(name = "direction") Direction direction) {
		Product example = new Product(null, null, null, null, categoryIds);
		return productService.findByExamplePageable(example, page, size, sortBy, direction);
	}
	
	@PutMapping("/assign/{productId}/toCategory/{categoryId}")
	@ApiOperation(value = "Assign given product to given category")
	public Mono<Product> assignToCategory(
			@ApiParam(value = "Id of product to be assigned to category") @PathVariable String productId,
			@ApiParam(value = "Id of category to assign a product") @PathVariable String categoryId) {
		return productService.assignToCategory(productId, categoryId);
	}
	
	@PutMapping("/remove/{productId}/fromCategory/{categoryId}")
	@ApiOperation(value = "Remove given product from given category")
	public Mono<Product> removeFromCategory(
			@ApiParam(value = "Id of product to be removed from category") @PathVariable String productId,
			@ApiParam(value = "Id of category to remove a product from") @PathVariable String categoryId) {
		return productService.removeFromCategory(productId, categoryId);
	}
}




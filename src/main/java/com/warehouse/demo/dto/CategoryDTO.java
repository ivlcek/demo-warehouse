package com.warehouse.demo.dto;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CategoryDTO implements Serializable, Comparable<CategoryDTO> {
	
	private static final long serialVersionUID = -1281797118955176246L;
	
	private String name;
	private String parent;
	private List<CategoryDTO> children = new ArrayList<>();
	
	public CategoryDTO(String name) {
		this.name = name;
	}
	
	@Override
	public int compareTo(CategoryDTO o) {
		return name.compareTo(o.name);
	}

}

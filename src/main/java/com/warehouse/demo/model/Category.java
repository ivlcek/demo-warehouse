package com.warehouse.demo.model;

import java.util.List;

//import javax.validation.constraints.NotNull;
//import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Document
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Category entity", description = "All details about the entity.")
public class Category {
	
	@Id
	@ApiModelProperty(value = "Category's unique name also serves as unique identifier.")
	private String name;
	
	@ApiModelProperty(value = "Category's parent name.")
	private String parent;
	
	@ApiModelProperty(value = "Category ancestor names.")
	private List<String> ancestors;
	
	public Category(String name) {
		this.name = name;
	}
}

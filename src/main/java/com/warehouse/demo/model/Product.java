package com.warehouse.demo.model;

import java.math.BigDecimal;
import java.util.List;

//import javax.validation.constraints.NotEmpty;
//import javax.validation.constraints.NotNull;
//
//import org.hibernate.validator.constraints.Length;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Document
@NoArgsConstructor
@AllArgsConstructor
@ApiModel(value = "Product entity", description = "All details about the entity.")
public class Product {
	
	@Id
	@ApiModelProperty(value = "Product's id is a unique product identifier")
	private String id;
	
	@ApiModelProperty(value = "Product's name holds the information about product title")
	private String name;
	
	@ApiModelProperty(notes = "Product's description.")
	private String description;	
	
	@ApiModelProperty(value = "Product's price.")
	private BigDecimal price;
	
	@ApiModelProperty(value = "Categories attached to this Product. Categories are stored as a list of Category ids/names.")
	private List<String> categories;
	
	public Product(String id) {
		this.id = id;
	}
}

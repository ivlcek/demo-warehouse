package com.warehouse.demo.repository;

import java.util.Set;

import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.warehouse.demo.model.Category;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

public interface CategoryRepository extends ReactiveCrudRepository<Category, String> {

	Flux<Category> findDistinctByAncestorsIn(Set<String> ancestors);

	Flux<Category> findByNameContainingIgnoreCase(String name);
	
	Mono<Long> countDistinctByAncestorsIn(Set<String> categoryNames);
	
	Mono<Long> countDistinctByAncestors(String categoryName);
	
	Mono<Category> findByName(String name);
	
	Flux<Category> findByParent(String parent);
}
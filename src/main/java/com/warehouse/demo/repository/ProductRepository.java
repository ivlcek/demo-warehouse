package com.warehouse.demo.repository;

import org.springframework.data.repository.query.ReactiveQueryByExampleExecutor;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;

import com.warehouse.demo.model.Product;

import reactor.core.publisher.Flux;

public interface ProductRepository extends ReactiveCrudRepository<Product, String>, 
		ReactiveQueryByExampleExecutor<Product> {
	
	Flux<Product> findByCategoriesContaining(String categoryId);
}


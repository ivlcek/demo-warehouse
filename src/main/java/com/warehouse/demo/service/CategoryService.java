package com.warehouse.demo.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.warehouse.demo.dto.CategoryDTO;
import com.warehouse.demo.model.Category;
import com.warehouse.demo.repository.CategoryRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class CategoryService {
	
    private ProductService productService;
    private CategoryRepository categoryRepository;
    
	@Autowired
	public void setProductService(ProductService productService) {
	    this.productService = productService;
	}	
	
	@Autowired
	public void setCategoryRepository(CategoryRepository categoryRepository) {
	    this.categoryRepository = categoryRepository;
	}		
	
	/**
	 * Method retrieves ancestors (or path) for given category name. 
	 * Including category itself.
	 * 
	 * @param categoryName name of category which ancestors or path will be retrieved.
	 * @return Flux of Category
	 */
	public Flux<Category> getAncestorsIncluded(String categoryName) {
		return categoryRepository.findById(categoryName)
			.flatMapMany(category -> Flux.fromIterable(category.getAncestors()))
			.concatWithValues(categoryName)
			.map(this::convertIdToCategory);
	}
	
	/**
	 * Method retrieves ancestors (or path) for given category names. 
	 * Including categories themselves.
	 * 
	 * @param categoryNames of categories which ancestors or paths will be retrieved.
	 * @return Flux of Category
	 */
	public Flux<Category> getAncestorsIncluded(Set<String> categoryNames) {
		return Flux.merge(this.getAncestorsIdsExcluded(categoryNames), Flux.fromIterable(categoryNames))
				.distinct()
				.map(this::convertIdToCategory);
	}
	
	/**
	 * Method retrieves ancestorsIds of categoryName passed as input parameter.
	 * Excluding category itself.
	 * Since result contains ancestors for single category there should be no duplicates.
	 * 
	 * @param categoryName whose ancestorIds will be retrieved
	 * @return distinct Flux of ancestorsIds
	 */	
	public Flux<String> getAncestorsIdsExcluded(String categoryName) {		
		return categoryRepository.findById(categoryName)
			.flatMapMany(category -> Flux.fromIterable(category.getAncestors()));
	}		
	
	/**
	 * Method retrieves distinct ancestorsIds of all categoryNames passed as input parameter.
	 * Excluding categories themselves.
	 * Duplicates are removed from results including input categories themselves.
	 * 
	 * @param categoryNames whose ancestorIds will be retrieved
	 * @return distinct Flux of ancestorsIds
	 */		
	public Flux<String> getAncestorsIdsExcluded(Set<String> categoryNames) {		
		return categoryRepository.findAllById(categoryNames)
			.flatMap(category -> Flux.fromIterable(category.getAncestors()))			
			.distinct();
	}
	
	/**
	 * Method retrieves ancestors (or path) for given category names. Duplicates are removed from results. 
	 * Excluding categories themselves.
	 * 
	 * @param categoryNames names of categories which ancestors or paths will be retrieved.
	 * @return Flux of Category
	 */
	public Flux<Category> getAncestorsExcluded(Set<String> categoryNames) {
		return this.getAncestorsIdsExcluded(categoryNames)
				.map(this::convertIdToCategory);
	}		
	
	/**
	 * Method retrieves ancestors (or path) for given category name. Excluding given category itself.
	 * 
	 * @param categoryName name of category of which ancestors or paths will be retrieved.
	 * @return Flux of Category
	 */
	public Flux<Category> getAncestorsExcluded(String categoryName) {
		return this.getAncestorsIdsExcluded(categoryName)
//				.map(ancestorId -> new Category(ancestorId));
				.map(this::convertIdToCategory);
	}
	
	/**
	 * Method retrieves descendants for given category name
	 * 
	 * @param categoryName name of which all descendants be retrieved.
	 * @return Flux of Category
	 */
	public Flux<Category> getDescendants(String categoryName) {
		return categoryRepository.findDistinctByAncestorsIn(Collections.singleton(categoryName));
	}
	
	/**
	 * Method retrieves descendants for given category names
	 * 
	 * @param categoryNames names of which all descendants be retrieved.
	 * @return Flux of Category
	 */
	public Flux<Category> getDescendants(Set<String> categoryNames) {	
		return categoryRepository.findDistinctByAncestorsIn(categoryNames);
	}
	
	/**
	 * Method retrieves ancestors, descendants and the category itself for given category names.
	 * 
	 * @param categoryNames names of which ancestors and descendants will be retrieved.
	 * @return Flux of Category 
	 */
	public Flux<Category> getDescendantsAndAncestors(Set<String> categoryNames) {
		return getDescendants(categoryNames)
				.concatWith(getAncestorsIncluded(categoryNames))
				.distinct(Category::getName);
	}		
	
	/**
	 * Method retrieves ancestors, descendants and the category itself for given category name.
	 * 
	 * @param categoryName name of which ancestors and descendants will be retrieved.
	 * @return Flux of Category
	 */
	public Flux<Category> getDescendantsAndAncestors(String categoryName) {
		return getDescendantsAndAncestors(Collections.singleton(categoryName));
	}
	
	/**
	 * Method retrieves descendants count for given category name
	 * 
	 * @param categoryName name of which ancestors or paths will be retrieved.
	 * @return Mono of Long
	 */
	public Mono<Long> getDescendantsCount(String categoryName) {
		return categoryRepository.countDistinctByAncestors(categoryName);
	}	
	

	/**
	 * Method adds new Category in a tree structure based on provided parent category.
	 * 
	 * @param category new category to be added.
	 * @param parentCategoryName parent category where this category will be added as child.
	 * @return persisted Mono<Category> 
	 */
	public Mono<Category> addCategory(Category category, String parentCategoryName) {
		return categoryRepository.findById(parentCategoryName)
				.map(Category::getAncestors)
				.map(ancestors -> {
					ancestors.add(parentCategoryName);
					Category newCategory = new Category(category.getName(), parentCategoryName, ancestors);
					return newCategory;
				})
				.flatMap(categoryRepository::save);
	}
	
	/**
	 * Remove category and all its descendants. Remove all these sub-categories from all Products.
	 * 
	 * @param categoryName name of category to be removed.
	 * @return Mono<Void>
	 */
	public Mono<Void> removeCategory(String categoryName) {
		return this.getDescendants(categoryName).concatWith(categoryRepository.findById(categoryName))
			.flatMap(category -> productService.findByCategoriesContaining(category.getName())
					.flatMap(product -> productService.removeFromCategory(product.getId(), category.getName()))
			)
			.then(categoryRepository.deleteAll(this.getDescendants(categoryName)))
			.then(categoryRepository.deleteById(categoryName));
	}
	
	/**
	 * Find category containing text passed as input parameter
	 * 
	 * @param categoryName name of category to be found
	 * @return Flux of Category
	 */
	public Flux<Category> findByNameContainingIgnoreCase(String categoryName) {
		return categoryRepository.findByNameContainingIgnoreCase(categoryName);
	}	
	
	/**
	 * Find all categories
	 * 
	 * @return Flux of Category
	 */
	public Flux<Category> findAll() {
		return categoryRepository.findAll();
	}
	
	/**
	 * Find category by given unique name
	 * @param name of the category
	 * @return Mono<Category> 
	 */
	public Mono<Category> findByName(String name) {
		return categoryRepository.findByName(name);
	}	
	
	/**
	 * Helper method to convert an Id i.e. name to Category entity with this name as Id.
	 *  
	 * @param id name of the Category
	 * @return Category object
	 */
	private Category convertIdToCategory(String id) {
		return new Category(id);
	}
	
	/**
	 * Construct a tree structure from given category name
	 * 
	 * @param categoryName that will be the root of constructed tree
	 * @return category tree
	 */
	public Mono<List<CategoryDTO>> getTree(String categoryName) {
		return categoryRepository.findById(categoryName)
			.expandDeep(c -> categoryRepository.findByParent(c.getName()))
			.map(c -> new CategoryDTO(c.getName(), c.getParent(), Arrays.asList()))
			.collectList()
			.zipWith(categoryRepository.findById(categoryName))
			.map(tuple -> createTree(tuple.getT1(), tuple.getT2().getParent()));
	}
	
	/**
	 * Recursive method to construct categoryDTO that allows a tree structure.
	 * 
	 * @param categories to create a tree
	 * @param parentId of subtree
	 * @return subtree structure
	 */
	private List<CategoryDTO> createTree(final List<CategoryDTO> categories, String parentName) {
    	List<CategoryDTO> siblings = new ArrayList<>();
        categories.forEach(category -> {
            if (Objects.equals(category.getParent() == null ? null : category.getParent(), parentName)) {
                List<CategoryDTO> children = createTree(categories, category.getName());
                children.sort(CategoryDTO::compareTo);
                category.setChildren(children);
                siblings.add(category);
            }
        });
        return siblings;
    }	
}

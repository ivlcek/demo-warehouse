package com.warehouse.demo.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.domain.Sort.NullHandling;
import org.springframework.data.mongodb.core.ReactiveMongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import com.warehouse.demo.model.Product;
import com.warehouse.demo.repository.ProductRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@Service
public class ProductService {

	private final CategoryService categoryService;
	private final ProductRepository productRepository;
	private final ReactiveMongoOperations operations;

	public ProductService(
			ReactiveMongoOperations operations,
			CategoryService categoryService,
			ProductRepository productRepository) {
		this.operations = operations;
		this.categoryService = categoryService;
		this.productRepository = productRepository;
	}
	
	/**
	 * Find all products
	 * @return Flux of products
	 */
	public Flux<Product> findAll() {
		return productRepository.findAll();
	}
	
	/**
	 * Method finds product based on given Id.
	 * @param id of product
	 * @return found product
	 */
	public Mono<Product> findById(String id) {
		return productRepository.findById(id);
	}

	/**
	 * Method will find all products based on provided example in page-able format. This method uses mongoOperations.
	 * 
	 * @param product example based on which a search will be carried out
	 * @param page page-able parameter
	 * @param size page-able parameter
	 * @param sortBy page-able parameter
	 * @param direction page-able parameter
	 * @return Flux of products
	 */
	public Flux<Product> findByExamplePageable(Product product, int page, int size, String sortBy, Direction direction) {
		Query query = prepareQuery(product, page, size, sortBy, direction)
			.with(PageRequest.of(page, size, Sort.by(new Sort.Order(direction, sortBy, NullHandling.NULLS_LAST))));
		
		return Mono.just(query)
				.flatMap(q -> addCategoryCriteria(q, product))		
				.flatMapMany(q -> operations.find(q, Product.class));
	}
	
	/**
	 * Method will find all products based on provided example in page-able format and return their count.
	 * 
	 * @param product example based on which a search will be carried out
	 * @param page page-able parameter
	 * @param size page-able parameter
	 * @param sortBy page-able parameter
	 * @param direction page-able parameter
	 * @return Count of fetched products
	 */	
	public Mono<Long> findByExamplePageableCount(Product product, int page, int size, String sortBy, Direction direction) {
		Query query = prepareQuery(product, page, size, sortBy, direction)
			.with(PageRequest.of(page, size, Sort.by(new Sort.Order(direction, sortBy, NullHandling.NULLS_LAST))));
		
		return Mono.just(query)
				.flatMap(q -> addCategoryCriteria(q, product))			
				.flatMap(q -> operations.count(q, Product.class));
	}	
	
	/**
	 * Method will prepare query based on provided Product example. This query will be used to in two methods to retrieve 
	 * Products and count of fetched Products
	 * 
	 * @param product example based on which a search will be carried out
	 * @param page page-able parameter
	 * @param size page-able parameter
	 * @param sortBy page-able parameter
	 * @param direction page-able parameter
	 * @return Query configured with Product example attributes
	 */
	private Query prepareQuery(Product product, int page, int size, String sortBy, Direction direction) {
		Query query = new Query();
		if (product.getName() != null)
			query.addCriteria(Criteria.where("name").regex(product.getName(), "i"));
		if (product.getDescription() != null)
			query.addCriteria(Criteria.where("description").regex(product.getDescription(), "i"));
		if (product.getPrice() != null)
			query.addCriteria(Criteria.where("price").is(product.getPrice()));		
		return query;
	}

	/**
	 * Method saves new product or updates existing product.
	 * 
	 * @param product to be saved or updated.
	 * @return persisted Mono of product
	 */
	public Mono<Product> save(Product product) {
		return productRepository.save(product);
	}

	/**
	 * Method deletes products based on given list of ids.
	 * 
	 * @param ids of products to be deleted
	 * @return Mono<Void>
	 */
	public Mono<Void> deleteByIds(List<String> ids) {
		List<Product> products = new ArrayList<>();
		ids.forEach(id -> products.add(new Product(id)));		
		return productRepository.deleteAll(products);
	}	
	
	/**
	 * Method assigns a category to given product.
	 * 
	 * @param productId to be updated
	 * @param categoryId to be added to product
	 * @return updated product
	 */
	public Mono<Product> assignToCategory(String productId, String categoryId) {
		return productRepository.findById(productId)
			.map(product -> {
				product.getCategories().add(categoryId);
				return product;
			})
			.flatMap(productRepository::save);
	}
	
	/**
	 * Method removes product from given category.
	 * 
	 * @param productId to be updated
	 * @param categoryId to be removed from product
	 * @return updated product
	 */
	public Mono<Product> removeFromCategory(String productId, String categoryId) {
		return productRepository.findById(productId)
			.map(product -> {
				product.getCategories().remove(categoryId);
				return product;
			})
			.flatMap(productRepository::save);
	}		
	
	/**
	 * Helper method that adds Criteria to Query object if product example contains a categories
	 * 
	 * @param q query to be updated based on provided Product
	 * @param product example that is used to construct Query object
	 * @return Mono of Query with added criteria
	 */
	private Mono<Query> addCategoryCriteria(Query q, Product product) {
		if (product.getCategories() != null && !product.getCategories().isEmpty()) {
			return categoryService.getDescendantsAndAncestors(new HashSet<>(product.getCategories()))
				.collectList()
				.flatMap(list -> {
					List<String> ids = new ArrayList<>();
					list.forEach(category -> ids.add(category.getName()));
					return Mono.just(ids);
				})
				.flatMap(ids -> {
					q.addCriteria(Criteria.where("categories").in(ids));
					return Mono.just(q);
				});
		} else {
			return Mono.just(q);
		}										
	}
	
	/**
	 * Find all Products that contain given category name.
	 * @param categoryId name of category that is being searched in products
	 * @return Flux of products
	 */
	Flux<Product> findByCategoriesContaining(String categoryId) {
		return productRepository.findByCategoriesContaining(categoryId);
	}
}

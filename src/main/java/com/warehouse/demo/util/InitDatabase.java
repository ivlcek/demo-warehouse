package com.warehouse.demo.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.stereotype.Component;

import com.warehouse.demo.model.Category;
import com.warehouse.demo.model.Product;

/**
 * Component inserts some data into embedded Database at application startup.
 */
@Component
public class InitDatabase {
	@Bean
	CommandLineRunner init(MongoOperations mongoOperations) {
		return args -> {
			mongoOperations.dropCollection(Category.class);
			mongoOperations.insert(new Category("Android", "Phones", (Arrays.asList("All", "Security", "Phones"))));
			mongoOperations.insert(new Category("iPhone", "Phones", (Arrays.asList("All", "Security", "Phones"))));
			mongoOperations.insert(new Category("Phones", "Security", (Arrays.asList("All", "Security"))));
			mongoOperations.insert(new Category("Mac", "Computers", (Arrays.asList("All", "Security", "Computers"))));
			mongoOperations.insert(new Category("Computers", "Security", (Arrays.asList("All", "Security"))));
			mongoOperations.insert(new Category("Security", "All", (Arrays.asList("All"))));
			mongoOperations.insert(new Category("All", null, new ArrayList<>()));
			mongoOperations.findAll(Category.class).forEach(category -> {
				System.out.println(category.toString());
			});
			
			mongoOperations.dropCollection(Product.class);
			mongoOperations.insert(new Product("1", "Free Antivirus", "Free Antivirus for Mac computers", BigDecimal.ZERO, Arrays.asList("Mac")));
			mongoOperations.insert(new Product("2", "Premium Phone Security", "Premium Security product for all Mobile Phones", BigDecimal.TEN, Arrays.asList("Phones")));
			mongoOperations.insert(new Product("3", "Ultimate", "Ultimate security product for all devices", BigDecimal.TEN, Arrays.asList("Security")));
			mongoOperations.findAll(Product.class).forEach(product -> {
				System.out.println(product.toString());
			});
		};
	}
}
package com.warehouse.demo.controller;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Arrays;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.warehouse.demo.model.Category;

import reactor.core.publisher.Mono;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class CategoryControllerIntegrationTests {
	
	private WebTestClient webClient;
	
	@Autowired
	public void setup(ApplicationContext context) {
		this.webClient = WebTestClient
				.bindToApplicationContext(context)
				.configureClient()
				.build();
	}
	
	@Test
	public void aTestFindByName() {
		// when
		EntityExchangeResult<String> result = webClient
			.get().uri("/category/findByName/Security")
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		assertThat(result.getResponseBody())
		.contains("\"name\":\"Security\",\"parent\":\"All\",\"ancestors\":[\"All\"]");		
	}
	
	@Test
	public void bTestDeleteByName() {
		// when
		webClient
			.delete().uri("/category/deleteByName/Phones")
			.exchange()
			.expectStatus().isOk();
		
		// then
		EntityExchangeResult<String> deleted = webClient
			.get().uri("/category/findByName/Phones")
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		assertThat(deleted.getResponseBody()).isNullOrEmpty();		

		// then
		EntityExchangeResult<String> result = webClient
			.get().uri("/product/findAll")
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then check the Phones category was removed from Premium Phone Security product
		assertThat(result.getResponseBody())
			.contains("\"id\":\"1\",\"name\":\"Free Antivirus\",\"description\":\"Free Antivirus for Mac computers\",\"price\":0,\"categories\":[\"Mac\"]")
			.contains("\"id\":\"2\",\"name\":\"Premium Phone Security\",\"description\":\"Premium Security product for all Mobile Phones\",\"price\":10,\"categories\":[]")
			.contains("\"id\":\"3\",\"name\":\"Ultimate\",\"description\":\"Ultimate security product for all devices\",\"price\":10,\"categories\":[\"Security\"]");
	}
	
	@Test
	public void cTestAddCategory() {
		Category phones = new Category("Phones", "Security", (Arrays.asList("All", "Security")));
		
		// when
		EntityExchangeResult<String> result = webClient
			.put().uri("/category/addCategory")
			.body(Mono.just(phones), Category.class)
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		assertThat(result.getResponseBody())
		.contains("\"name\":\"Phones\",\"parent\":\"Security\",\"ancestors\":[\"All\",\"Security\"]");
		
		// assign Phones category to product Premium again
		webClient
			.put().uri("/product/assign/2/toCategory/Phones")
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();	
		
		// check updated product was persisted
		EntityExchangeResult<String> premiumUpdated = webClient
				.get().uri("/product/findById/2")
				.exchange()
				.expectStatus().isOk()
				.expectBody(String.class)
				.returnResult();
		// then
		assertThat(premiumUpdated.getResponseBody())
			.contains("\"id\":\"2\",\"name\":\"Premium Phone Security\",\"description\":\"Premium Security product for all Mobile Phones\",\"price\":10,\"categories\":[\"Phones\"]");
		
	}	
}

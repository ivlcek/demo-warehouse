package com.warehouse.demo.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.warehouse.demo.model.Category;
import com.warehouse.demo.service.CategoryService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RunWith(SpringRunner.class)
@WebFluxTest(controllers = CategoryController.class)
public class CategoryControllerTests {

	@Autowired
	WebTestClient webClient;

	@MockBean
	CategoryService categoryService;
	
	@Test
	public void testFindAllCategories() {
		// given
		Category freeMac = new Category("Free", "Mac", Arrays.asList("All", "Computers", "Mac"));
		Category premiumMac = new Category("Premium", "Mac", Arrays.asList("All", "Computers", "Mac"));
		given(categoryService.findAll())
			.willReturn(Flux.just(freeMac, premiumMac));

		// when
		EntityExchangeResult<String> result = webClient
			.get().uri("/category/findAll")
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		verify(categoryService).findAll();
		verifyNoMoreInteractions(categoryService);
		assertThat(result.getResponseBody())
			.contains("\"name\":\"Free\",\"parent\":\"Mac\",\"ancestors\":[\"All\",\"Computers\",\"Mac\"]")
			.contains("\"name\":\"Premium\",\"parent\":\"Mac\",\"ancestors\":[\"All\",\"Computers\",\"Mac\"]");
	}	
	
	@Test
	public void testFindByName() {
		// given
		given(categoryService.findByName("Free"))
			.willReturn(Mono.just(new Category("Free", "Mac", Arrays.asList("All", "Computers", "Mac"))));

		// when
		EntityExchangeResult<String> result = webClient
			.get().uri("/category/findByName/Free")
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		verify(categoryService).findByName("Free");
		verifyNoMoreInteractions(categoryService);
		assertThat(result.getResponseBody())
			.contains("\"name\":\"Free\",\"parent\":\"Mac\",\"ancestors\":[\"All\",\"Computers\",\"Mac\"]");

	}	
	
	@Test
	public void testFindByNameContainingIgnoreCase() {
		// given
		given(categoryService.findByNameContainingIgnoreCase("se"))
			.willReturn(Flux.just(
					new Category("Free Security", "Mac", Arrays.asList("All", "Computers", "Mac")),
					new Category("Ultimate Security", "Mac", Arrays.asList("All", "Computers", "Mac")),
					new Category("Unsecured Browser", "Mac", Arrays.asList("All", "Computers", "Mac"))
					));
		
		// when
		EntityExchangeResult<String> result = webClient
				.get().uri("/category/findByNameContainingIgnoreCase?name=se")
				.exchange()
				.expectStatus().is2xxSuccessful()
				.expectBody(String.class)
				.returnResult();
		
		// then
		verify(categoryService).findByNameContainingIgnoreCase("se");
		verifyNoMoreInteractions(categoryService);
		assertThat(result.getResponseBody())
			.contains("\"name\":\"Free Security\",\"parent\":\"Mac\",\"ancestors\":[\"All\",\"Computers\",\"Mac\"]")
			.contains("\"name\":\"Ultimate Security\",\"parent\":\"Mac\",\"ancestors\":[\"All\",\"Computers\",\"Mac\"]")
			.contains("\"name\":\"Unsecured Browser\",\"parent\":\"Mac\",\"ancestors\":[\"All\",\"Computers\",\"Mac\"]");
	}		

	@Test
	public void testAddCategory() {
		Category category = new Category("Free Security", "Mac", Arrays.asList("All", "Computers", "Mac"));
		
		// given
		given(categoryService.addCategory(category, category.getParent()))
			.willReturn(Mono.just(
					new Category("Free Security", "Mac", Arrays.asList("All", "Computers", "Mac"))
					));
		
		// when
		EntityExchangeResult<String> result = webClient
				.put().uri("/category/addCategory")
				.body(Mono.just(category), Category.class)
				.exchange()
				.expectStatus().is2xxSuccessful()
				.expectBody(String.class)
				.returnResult();

		// then
		verify(categoryService).addCategory(category, category.getParent());
		verifyNoMoreInteractions(categoryService);
		assertThat(result.getResponseBody())
			.contains("\"name\":\"Free Security\",\"parent\":\"Mac\",\"ancestors\":[\"All\",\"Computers\",\"Mac\"]");
	}
}

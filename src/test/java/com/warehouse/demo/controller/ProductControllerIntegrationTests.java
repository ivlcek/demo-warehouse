package com.warehouse.demo.controller;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.warehouse.demo.model.Product;

import reactor.core.publisher.Mono;

@RunWith(SpringRunner.class)
@SpringBootTest
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class ProductControllerIntegrationTests {
	
	private WebTestClient webClient;
	
	@Autowired
	public void setup(ApplicationContext context) {
		this.webClient = WebTestClient
				.bindToApplicationContext(context)
				.configureClient()
				.build();
	}
	
	@Test
	public void aTestFindAllProducts() {
		// when
		EntityExchangeResult<String> result = webClient
			.get().uri("/product/findAll")
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		assertThat(result.getResponseBody())
			.contains("\"id\":\"1\",\"name\":\"Free Antivirus\",\"description\":\"Free Antivirus for Mac computers\",\"price\":0,\"categories\":[\"Mac\"]")
			.contains("\"id\":\"2\",\"name\":\"Premium Phone Security\",\"description\":\"Premium Security product for all Mobile Phones\",\"price\":10,\"categories\":[\"Phones\"]")
			.contains("\"id\":\"3\",\"name\":\"Ultimate\",\"description\":\"Ultimate security product for all devices\",\"price\":10,\"categories\":[\"Security\"]");
	}
	
	@Test
	public void bTestFindById() {
		// when
		EntityExchangeResult<String> result = webClient
			.get().uri("/product/findById/3")
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		assertThat(result.getResponseBody())
			.contains("\"id\":\"3\",\"name\":\"Ultimate\",\"description\":\"Ultimate security product for all devices\",\"price\":10,\"categories\":[\"Security\"]");
	}	
	
	@Test
	public void cTestAddNewProduct() {
		Product spyware = new Product("4", "Spyware product", "Spyware tool for Mac computers", BigDecimal.TEN, Arrays.asList("Mac"));
		
		// when
		EntityExchangeResult<String> result = webClient
			.put().uri("/product/save")
			.body(Mono.just(spyware), Product.class)
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		assertThat(result.getResponseBody())
			.contains("\"id\":\"4\",\"name\":\"Spyware product\",\"description\":\"Spyware tool for Mac computers\",\"price\":10,\"categories\":[\"Mac\"]");
	}
	
	@Test
	public void dTestDeleteById() {
		// when
		webClient
			.delete().uri("/product/deleteById/4")
			.exchange()
			.expectStatus().isOk();

		// when
		EntityExchangeResult<String> result = webClient
			.get().uri("/product/findAll")
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		assertThat(result.getResponseBody())
			.contains("\"id\":\"1\",\"name\":\"Free Antivirus\",\"description\":\"Free Antivirus for Mac computers\",\"price\":0,\"categories\":[\"Mac\"]")
			.contains("\"id\":\"2\",\"name\":\"Premium Phone Security\",\"description\":\"Premium Security product for all Mobile Phones\",\"price\":10,\"categories\":[\"Phones\"]")
			.contains("\"id\":\"3\",\"name\":\"Ultimate\",\"description\":\"Ultimate security product for all devices\",\"price\":10,\"categories\":[\"Security\"]")
			.doesNotContain("\"id\":\"4\",\"name\":\"Spyware product\",\"description\":\"Spyware tool for Mac computers\",\"price\":10,\"categories\":[\"Mac\"]");			
	}
	
	@Test
	public void eTestFindByExample() {
		Product example = new Product(null, "m", null, null, Arrays.asList("Security"));
		
		// when
		EntityExchangeResult<String> result = webClient
			.post().uri("/product/findByExamplePageable?page=0&size=20&sortBy=id&direction=ASC")
			.body(Mono.just(example), Product.class)
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		assertThat(result.getResponseBody())
			.doesNotContain("\"id\":\"1\",\"name\":\"Free Antivirus\",\"description\":\"Free Antivirus for Mac computers\",\"price\":0,\"categories\":[\"Mac\"]")
			.contains("\"id\":\"2\",\"name\":\"Premium Phone Security\",\"description\":\"Premium Security product for all Mobile Phones\",\"price\":10,\"categories\":[\"Phones\"]")
			.contains("\"id\":\"3\",\"name\":\"Ultimate\",\"description\":\"Ultimate security product for all devices\",\"price\":10,\"categories\":[\"Security\"]");
	}
	
	@Test
	public void eTestFindByExampleNotFound() {
		Product example = new Product(null, "m", null, null, Arrays.asList("Mac"));
		
		// when
		EntityExchangeResult<String> result = webClient
			.post().uri("/product/findByExamplePageable?page=0&size=20&sortBy=id&direction=ASC")
			.body(Mono.just(example), Product.class)
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		assertThat(result.getResponseBody())
			.doesNotContain("\"id\":\"1\",\"name\":\"Free Antivirus\",\"description\":\"Free Antivirus for Mac computers\",\"price\":0,\"categories\":[\"Mac\"]")
			.doesNotContain("\"id\":\"2\",\"name\":\"Premium Phone Security\",\"description\":\"Premium Security product for all Mobile Phones\",\"price\":10,\"categories\":[\"Phones\"]")
			.contains("\"id\":\"3\",\"name\":\"Ultimate\",\"description\":\"Ultimate security product for all devices\",\"price\":10,\"categories\":[\"Security\"]");
	}	
	
	@Test
	public void fTestFindByCategories() {
		// when
		EntityExchangeResult<String> result = webClient
			.post().uri("/product/findByCategories?page=0&size=20&sortBy=id&direction=ASC")
			.body(Mono.just(Arrays.asList("Phones")), Product.class)
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		assertThat(result.getResponseBody())
			.doesNotContain("\"id\":\"1\",\"name\":\"Free Antivirus\",\"description\":\"Free Antivirus for Mac computers\",\"price\":0,\"categories\":[\"Mac\"]")
			.contains("\"id\":\"2\",\"name\":\"Premium Phone Security\",\"description\":\"Premium Security product for all Mobile Phones\",\"price\":10,\"categories\":[\"Phones\"]")
			.contains("\"id\":\"3\",\"name\":\"Ultimate\",\"description\":\"Ultimate security product for all devices\",\"price\":10,\"categories\":[\"Security\"]");
	}	
	
	@Test
	public void gTestAssignToCategory() {
		// when
		EntityExchangeResult<String> result = webClient
			.put().uri("/product/assign/1/toCategory/iPhone")
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		assertThat(result.getResponseBody())
			.contains("\"id\":\"1\",\"name\":\"Free Antivirus\",\"description\":\"Free Antivirus for Mac computers\",\"price\":0,\"categories\":[\"Mac\",\"iPhone\"]");
		
		// check updated product was persisted
		EntityExchangeResult<String> updated = webClient
				.get().uri("/product/findById/1")
				.exchange()
				.expectStatus().isOk()
				.expectBody(String.class)
				.returnResult();
		// then
		assertThat(updated.getResponseBody())
			.contains("\"id\":\"1\",\"name\":\"Free Antivirus\",\"description\":\"Free Antivirus for Mac computers\",\"price\":0,\"categories\":[\"Mac\",\"iPhone\"]");
	}	
	
	@Test
	public void gTestRemoveFromCategory() {
		// when
		EntityExchangeResult<String> result = webClient
			.put().uri("/product/remove/1/fromCategory/iPhone")
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		assertThat(result.getResponseBody())
			.contains("\"id\":\"1\",\"name\":\"Free Antivirus\",\"description\":\"Free Antivirus for Mac computers\",\"price\":0,\"categories\":[\"Mac\"]");
		
		// check updated product was persisted
		EntityExchangeResult<String> updated = webClient
				.get().uri("/product/findById/1")
				.exchange()
				.expectStatus().isOk()
				.expectBody(String.class)
				.returnResult();
		// then
		assertThat(updated.getResponseBody())
			.contains("\"id\":\"1\",\"name\":\"Free Antivirus\",\"description\":\"Free Antivirus for Mac computers\",\"price\":0,\"categories\":[\"Mac\"]");
	}
	
}

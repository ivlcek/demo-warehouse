package com.warehouse.demo.controller;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.EntityExchangeResult;
import org.springframework.test.web.reactive.server.WebTestClient;

import com.warehouse.demo.model.Product;
import com.warehouse.demo.service.ProductService;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RunWith(SpringRunner.class)
@WebFluxTest(controllers = ProductController.class)
public class ProductControllerTests {

	@Autowired
	WebTestClient webClient;

	@MockBean
	ProductService productService;
	
	@Test
	public void testFindAllProducts() {
		// given
		Product free = new Product("1", "Free Antivirus", "Free Antivirus for Mac computers", BigDecimal.ZERO, Arrays.asList("Mac"));
		Product premium = new Product("2", "Premium Phone Security", "Premium Security product for all Mobile Phones", BigDecimal.TEN, Arrays.asList("Phones"));
		Product ultimate = new Product("3", "Ultimate", "Ultimate security product for all devices", BigDecimal.TEN, Arrays.asList("Security"));
		given(productService.findAll())
			.willReturn(Flux.just(free, premium, ultimate));

		// when
		EntityExchangeResult<String> result = webClient
			.get().uri("/product/findAll")
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		verify(productService).findAll();
		verifyNoMoreInteractions(productService);
		assertThat(result.getResponseBody())
			.contains("\"id\":\"1\",\"name\":\"Free Antivirus\",\"description\":\"Free Antivirus for Mac computers\",\"price\":0,\"categories\":[\"Mac\"]")
			.contains("\"id\":\"2\",\"name\":\"Premium Phone Security\",\"description\":\"Premium Security product for all Mobile Phones\",\"price\":10,\"categories\":[\"Phones\"]")
			.contains("\"id\":\"3\",\"name\":\"Ultimate\",\"description\":\"Ultimate security product for all devices\",\"price\":10,\"categories\":[\"Security\"]");
	}	
	
	@Test
	public void testFindById() {
		// given
		Product free = new Product("1", "Free Antivirus", "Free Antivirus for Mac computers", BigDecimal.ZERO, Arrays.asList("Mac"));
		given(productService.findById("1"))
			.willReturn(Mono.just(free));

		// when
		EntityExchangeResult<String> result = webClient
			.get().uri("/product/findById/1")
			.exchange()
			.expectStatus().isOk()
			.expectBody(String.class)
			.returnResult();

		// then
		verify(productService).findById("1");
		verifyNoMoreInteractions(productService);
		assertThat(result.getResponseBody())
			.contains("\"id\":\"1\",\"name\":\"Free Antivirus\",\"description\":\"Free Antivirus for Mac computers\",\"price\":0,\"categories\":[\"Mac\"]");					  

	}	
	
	@Test
	public void testSaveProduct() {
		Product spyware = new Product("1", "Spyware product", "Spyware tool for Mac computers", BigDecimal.TEN, Arrays.asList("Mac"));
		
		// given
		given(productService.save(spyware))
			.willReturn(Mono.just(spyware));
		
		// when
		EntityExchangeResult<String> result = webClient
				.put().uri("/product/save")
				.body(Mono.just(spyware), Product.class)
				.exchange()
				.expectStatus().is2xxSuccessful()
				.expectBody(String.class)
				.returnResult();

		// then
		verify(productService).save(spyware);
		verifyNoMoreInteractions(productService);
		assertThat(result.getResponseBody())
			.contains("\"id\":\"1\",\"name\":\"Spyware product\",\"description\":\"Spyware tool for Mac computers\",\"price\":10,\"categories\":[\"Mac\"]");
	}
}

package com.warehouse.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.junit4.SpringRunner;

import com.warehouse.demo.model.Category;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

/**
 *  Before running tests we need to persist entities to our embedded MongoDB data store. 
 *  For such operations, it's recommended to actually use the blocking API. 
 *  That's because when launching an application, there is a certain risk 
 *  of a thread lock issue when both the web container as well as 
 *  our hand-written loader are starting up.
 */
@RunWith(SpringRunner.class)
@DataMongoTest
public class CategoryRepositoryTests {
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private MongoOperations mongoOperations;
	
	@Before
	public void setUp() {		
		mongoOperations.insert(new Category("Android", "Phones", (Arrays.asList("All", "Security", "Phones"))));
		mongoOperations.insert(new Category("iPhone", "Phones", (Arrays.asList("All", "Security", "Phones"))));
		mongoOperations.insert(new Category("Phones", "Security", (Arrays.asList("All", "Security"))));
		mongoOperations.insert(new Category("Mac", "Computers", (Arrays.asList("All", "Security", "Computers"))));
		mongoOperations.insert(new Category("Computers", "Security", (Arrays.asList("All", "Security"))));
		mongoOperations.insert(new Category("Security", "All", (Arrays.asList("All"))));
		mongoOperations.insert(new Category("All", null, new ArrayList<>()));
		mongoOperations.findAll(Category.class).forEach(System.out::println);
	}
	
	@After
	public void cleanUp() {
		mongoOperations.dropCollection(Category.class);		
	}
	
	@Test
	public void testFindAll() {
		Flux<Category> categories = categoryRepository.findAll();

		StepVerifier.create(categories)
			.recordWith(ArrayList::new)
			.expectNextCount(7)
			.consumeRecordedWith(results -> {
				assertThat(results).hasSize(7);
				assertThat(results)
					.extracting(Category::getName)
					.contains(
						"All",
						"Security",
						"Phones",
						"Computers",
						"Android",
						"iPhone",
						"Mac");
			})
			.expectComplete()
			.verify();
	}
	
	@Test
	public void testFindById() {
		Mono<Category> category = categoryRepository.findById("iPhone");

		StepVerifier.create(category)
			.expectNextMatches(results -> {
					assertThat(results.getName()).isEqualTo("iPhone");
					assertThat(results.getParent()).isEqualTo("Phones");
					return true;
			});
	}
	
	@Test
	public void tesFindAncestors() {
		Mono<Category> category = categoryRepository.findById("iPhone");
		
		StepVerifier.create(category)
			.expectNextMatches(results -> {
				assertThat(results.getName()).isEqualTo("iPhone");
				assertThat(results.getAncestors().size() == 3);
				assertThat(results.getAncestors().containsAll(new HashSet<String>(Arrays.asList("All", "Security", "Phones"))));
				return true;
			});				
	}
	
	@Test
	public void testFindDescendants() {		
		Flux<Category> categories = categoryRepository.findDistinctByAncestorsIn(
				new HashSet<String>(Arrays.asList("Security")));
		
		StepVerifier.create(categories)
		.recordWith(ArrayList::new)
		.expectNextCount(5)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(5);
			assertThat(results)
				.extracting(Category::getName)
				.contains(
					"Phones",
					"Computers",
					"Android",
					"iPhone",
					"Mac");
		})
		.expectComplete()
		.verify();						
	}
	
	@Test
	public void testFindDescendantsMultipleInput() {
		Flux<Category> categories = categoryRepository.findDistinctByAncestorsIn(
				new HashSet<String>(Arrays.asList("Computers", "Phones")));
		
		StepVerifier.create(categories)
		.recordWith(ArrayList::new)
		.expectNextCount(3)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(3);
			assertThat(results)
				.extracting(Category::getName)
				.contains(
					"Android",
					"iPhone",
					"Mac");
		})
		.expectComplete()
		.verify();						
	}	
	
	/**
	 * Test retrieved Descendants and Ancestors including searched categories themselves.
	 * This method tests a concatenation of two Fluxes descendants and ancestors. 
	 */	
	@Test
	public void testFindDescendantsAndAncestorsSingleNode() {		
		Set<String> searchingCategories = new HashSet<>(Arrays.asList("Security"));
		Flux<Category> descendantsAndAscendants = getDescendantsAndAncestorsInternal(searchingCategories);
		
		StepVerifier.create(descendantsAndAscendants)
		.recordWith(ArrayList::new)
		.expectNextCount(7)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(7);
			assertThat(results)
				.extracting(Category::getName)
				.contains(
					"All",
					"Security",
					"Phones",
					"Computers",
					"Android",
					"iPhone",
					"Mac");
		})
		.expectComplete()
		.verify();			
	}
	
	/**
	 * Test retrieved Descendants and Ancestors including searched categories themselves.
	 * This method tests a concatenation of two Fluxes descendants and ancestors. 
	 */
	@Test
	public void findDescendantsAndAncestorsMultipleNodes() {
		Set<String> searchingCategories = new HashSet<>(Arrays.asList("Phones", "Computers"));
		Flux<Category> descendantsAndAscendants = getDescendantsAndAncestorsInternal(searchingCategories);
				
		StepVerifier.create(descendantsAndAscendants)
		.recordWith(ArrayList::new)
		.expectNextCount(7)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(7);
			assertThat(results)
				.extracting(Category::getName)
				.contains(
					"All",
					"Security",
					"Phones",
					"Computers",
					"Mac",
					"Android",
					"iPhone");
		})
		.expectComplete()
		.verify();			
	}	
	
	
	/**
	 * Find descendants and ancestors including searched category. Using reactive instead of for cycle
	 */
	@Test
	public void findDescendantsAndAncestorsRootNode() {
		Set<String> searchingCategories = new HashSet<>(Arrays.asList("All"));
		Flux<Category> descendantsAndAscendants = getDescendantsAndAncestorsInternal(searchingCategories);
		
		StepVerifier.create(descendantsAndAscendants)
		.recordWith(ArrayList::new)
		.expectNextCount(7)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(7);
			assertThat(results)
				.extracting(Category::getName)
				.contains(
						"All",
						"Security",
						"Phones",
						"Computers",
						"Mac",
						"Android",
						"iPhone");
		})
		.expectComplete()
		.verify();			
		
	}			
	
	@Test
	public void findDescendantsAndAncestorsListNode() {
		Set<String> searchingCategories = new HashSet<>(Arrays.asList("Mac"));
		Flux<Category> descendantsAndAscendants = getDescendantsAndAncestorsInternal(searchingCategories);
				
		StepVerifier.create(descendantsAndAscendants)
		.recordWith(ArrayList::new)
		.expectNextCount(4)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(4);
			assertThat(results)
				.extracting(Category::getName)
				.contains(
						"All",
						"Security",
						"Computers",
						"Mac");
		})
		.expectComplete()
		.verify();			
	}	
	
	/**
	 * This is only a helper method to verify concatenation of Fluxes works fine for various nodes
	 * in a tree structure. This or similar behavior will be used in a CategoryService class.
	 * 
	 * @param searchingCategories categories whose descendants and ancestors will be retrieved
	 * @return descendants and ancestors including searchingCategories themselves
	 */
	private Flux<Category> getDescendantsAndAncestorsInternal(Set<String> searchingCategories) {
		Flux<Category> descendants = categoryRepository.findDistinctByAncestorsIn(searchingCategories);
		Flux<Category> descendantsAndAscendants = Flux.fromIterable(searchingCategories)
			.flatMap(categoryRepository::findById)			
			.map(Category::getAncestors)
			.map(this::convertIdsToCategories)
			// include searchingCategories themselves into the final result
			.concatWithValues(convertIdsToCategories(searchingCategories))
			.flatMap(Flux::fromIterable)
			.groupBy(Category::getName)
			.flatMap(g -> g.reduce((a, b) -> a.getName().compareTo(b.getName()) > 0 ? a : b))
			.concatWith(descendants);	
		return descendantsAndAscendants;
	}
	
	private List<Category> convertIdsToCategories(List<String> ids) {
		List<Category> categories = new ArrayList<>();		
		ids.forEach(id -> categories.add(new Category(id)));
		return categories;
	}
	
	private List<Category> convertIdsToCategories(Set<String> ids) {
		List<Category> categories = new ArrayList<>();		
		ids.forEach(id -> categories.add(new Category(id)));
		return categories;
	}	
	
}
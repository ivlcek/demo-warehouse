package com.warehouse.demo.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.junit4.SpringRunner;

import com.warehouse.demo.dto.CategoryDTO;
import com.warehouse.demo.model.Category;
import com.warehouse.demo.repository.CategoryRepository;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CategoryServiceTests {
	
	@Autowired
	private CategoryService service;
	
	@Autowired
	private CategoryRepository categoryRepository;
	
	@Autowired
	private MongoOperations mongoOperations;

	@Before
	public void setUp() {	
		mongoOperations.dropCollection(Category.class);
		
		mongoOperations.insert(new Category("Android", "Phones", (Arrays.asList("All", "Security", "Phones"))));
		mongoOperations.insert(new Category("iPhone", "Phones", (Arrays.asList("All", "Security", "Phones"))));
		mongoOperations.insert(new Category("Phones", "Security", (Arrays.asList("All", "Security"))));
		mongoOperations.insert(new Category("Mac", "Computers", (Arrays.asList("All", "Security", "Computers"))));
		mongoOperations.insert(new Category("Computers", "Security", (Arrays.asList("All", "Security"))));
		mongoOperations.insert(new Category("Security","All", (Arrays.asList("All"))));
		mongoOperations.insert(new Category("All", null, new ArrayList<>()));
		mongoOperations.findAll(Category.class).forEach(System.out::println);
	}
	
	@After
	public void cleanUp() {
		mongoOperations.dropCollection(Category.class);
	}	
		
	@Test
	public void testGetAncestorsAll() {
		String categoryId = "All";
		Flux<Category> ancestors = service.getAncestorsIncluded(categoryId);
		
		StepVerifier.create(ancestors)
		.recordWith(ArrayList::new)
		.expectNextCount(1)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(1);
			assertThat(results)
			.extracting(Category::getName)
			.containsExactlyInAnyOrder(
				"All");
		})
		.expectComplete()
		.verify();		
	}
	
	
	@Test
	public void testGetAncestorsAllExcludingCategory() {
		String categoryId = "Mac";
		Flux<Category> ancestors = service.getAncestorsExcluded(categoryId);
		
		StepVerifier.create(ancestors)
		.recordWith(ArrayList::new)
		.expectNextCount(3)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(3);
			assertThat(results)
			.extracting(Category::getName)
			.containsExactlyInAnyOrder(
				"All", "Security", "Computers");			
		})
		.expectComplete()
		.verify();		
	}
	
	@Test
	public void testGetAncestorsMac() {
		String categoryId = "Mac";
		Flux<Category> ancestors = service.getAncestorsIncluded(categoryId);
		
		StepVerifier.create(ancestors)
		.recordWith(ArrayList::new)
		.expectNextCount(4)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(4);		
			assertThat(results)
			.extracting(Category::getName)
			.containsExactlyInAnyOrder(
				"Security",
				"Computers",
				"All",
				"Mac");
		})
		.expectComplete()
		.verify();		
	}	
	
	@Test
	public void testGetAncestorsMacExcludingCategory() {
		String categoryId = "Mac";
		Flux<Category> ancestors = service.getAncestorsExcluded(categoryId);
		
		StepVerifier.create(ancestors)
		.recordWith(ArrayList::new)
		.expectNextCount(3)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(3);		
			assertThat(results)
			.extracting(Category::getName)
			.contains(
				"Security",
				"Computers",
				"All");
		})
		.expectComplete()
		.verify();		
	}		
	
	@Test
	public void testGetAncestorsPhones() {
		String categoryId = "Phones";
		Flux<Category> ancestors = service.getAncestorsIncluded(categoryId);
		
		StepVerifier.create(ancestors)
		.recordWith(ArrayList::new)
		.expectNextCount(3)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(3);		
			assertThat(results)
			.extracting(Category::getName)
			.containsExactlyInAnyOrder(
				"Phones",
				"Security",
				"All");
		})
		.expectComplete()
		.verify();		
	}		
	
	@Test
	public void testGetDescendants() {
		String categoryId = "Phones";
		Flux<Category> descendants = service.getDescendants(categoryId);
		
		StepVerifier.create(descendants)
		.recordWith(ArrayList::new)
		.expectNextCount(2)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(2);		
			assertThat(results)
			.extracting(Category::getName)
			.contains(
				"Android",
				"iPhone");
		})
		.expectComplete()
		.verify();		
	}		
	
	@Test
	public void testGetDescendantsSet() {
		Set<String> categoryIds = new HashSet<>();
		categoryIds.add("Phones");
		categoryIds.add("Computers");
		Flux<Category> descendants = service.getDescendants(categoryIds);
		
		StepVerifier.create(descendants)
		.recordWith(ArrayList::new)
		.expectNextCount(3)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(3);		
			assertThat(results)
			.extracting(Category::getName)
			.contains(
				"Mac",
				"Android",
				"iPhone");
		})
		.expectComplete()
		.verify();		
	}		
	
	@Test
	public void testGetDescendantsSet2() {
						
		Set<String> categoryIds = new HashSet<>();
		categoryIds.add("iPhone");
		categoryIds.add("Computers");
		Flux<Category> descendants = service.getDescendants(categoryIds);
		
		StepVerifier.create(descendants)
		.recordWith(ArrayList::new)
		.expectNextCount(1)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(1);		
			assertThat(results)
			.extracting(Category::getName)
			.contains(
				"Mac");
		})
		.expectComplete()
		.verify();		
	}		
	
	@Test
	public void testGetDescendantsSet3() {
		Set<String> categoryIds = new HashSet<>();
		categoryIds.add("All");	
		Flux<Category> descendants = service.getDescendants(categoryIds);
		
		StepVerifier.create(descendants)
		.recordWith(ArrayList::new)
		.expectNextCount(6)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(6);		
			assertThat(results)
			.extracting(Category::getName)
			.contains(
				"Phones",
				"Android",
				"iPhone",					
				"Security",
				"Computers",
				"Mac");
		})
		.expectComplete()
		.verify();		
	}		
	
	@Test
	public void testGetDescendantsAndAncestors() {
		Set<String> categoryIds = new HashSet<>();
		categoryIds.add("All");	
		Flux<Category> descendants = service.getDescendantsAndAncestors(categoryIds);
		
		StepVerifier.create(descendants)
		.recordWith(ArrayList::new)
		.expectNextCount(7)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(7);		
			assertThat(results)
			.extracting(Category::getName)
			.contains(
				"Phones",
				"Android",
				"iPhone",					
				"Security",
				"Computers",
				"All",
				"Mac");
		})
		.expectComplete()
		.verify();		
	}	
	
	@Test
	public void testGetDescendantsAndAncestors2() {
		Set<String> categoryIds = new HashSet<>();
		categoryIds.add("Computers");	
		categoryIds.add("Phones");
		categoryIds.add("Mac");
		Flux<Category> descendants = service.getDescendantsAndAncestors(categoryIds);
		
		StepVerifier.create(descendants)
		.recordWith(ArrayList::new)
		.expectNextCount(7)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(7);		
			assertThat(results)
			.extracting(Category::getName)
			.contains(
				"Phones",
				"Android",
				"iPhone",					
				"Security",
				"Computers",
				"All",
				"Mac");
		})
		.expectComplete()
		.verify();		
	}	
	
	@Test
	public void testGetDescendantsAndAncestors3() {
		Set<String> categoryIds = new HashSet<>();
		categoryIds.add("Computers");	
		categoryIds.add("iPhone");
		Flux<Category> descendants = service.getDescendantsAndAncestors(categoryIds);
		
		StepVerifier.create(descendants)
		.recordWith(ArrayList::new)
		.expectNextCount(6)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(6);		
			assertThat(results)
			.extracting(Category::getName)
			.contains(
				"Phones",
				"iPhone",					
				"Security",
				"Computers",
				"All",
				"Mac");
		})
		.expectComplete()
		.verify();		
	}		
	
	@Test
	public void testGetDescendantsAndAncestors4() {
		Flux<Category> descendants = service.getDescendantsAndAncestors("iPhone");
		
		StepVerifier.create(descendants)
		.recordWith(ArrayList::new)
		.expectNextCount(4)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(4);		
			assertThat(results)
			.extracting(Category::getName)
			.contains(
				"Phones",
				"iPhone",					
				"Security",
				"All");
		})
		.expectComplete()
		.verify();		
	}			
	
	@Test
	public void testGetDescendantsAndAncestors5() {
		Flux<Category> descendants = service.getDescendantsAndAncestors("Phones");
		
		StepVerifier.create(descendants)
		.recordWith(ArrayList::new)
		.expectNextCount(5)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(5);		
			assertThat(results)
			.extracting(Category::getName)
			.contains(
				"Phones",
				"iPhone",	
				"Android", 	
				"Security",
				"All");
		})
		.expectComplete()
		.verify();		
	}			
	
	@Test
	public void testRemoveAddCategory() {
		Mono<Category> storedCategory = service.addCategory(new Category("Windows"), "Phones");
		
		StepVerifier.create(storedCategory)
		.expectNextMatches(c -> {
			assertThat(c.getName()).isNotNull();
			assertThat(c.getName()).isEqualTo("Windows");
			assertThat(c.getParent()).isEqualTo("Phones");
			assertThat(c.getAncestors()).hasSize(3);
			assertThat(c.getAncestors()).contains("All", "Security", "Phones");
			return true;
		})
		.expectComplete()
		.verify();
	}
	
	@Test
	public void testRemoveCategory() {
		Mono<Void> removedCategory = service.removeCategory("Windows");
		
		StepVerifier.create(removedCategory)
		.expectComplete()
		.verify();
		
		Flux<Category> all = categoryRepository.findAll();
		
		StepVerifier.create(all)
		.recordWith(ArrayList::new)
		.expectNextCount(7)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(7);		
			assertThat(results)
			.extracting(Category::getName)
			.contains(
				"Computers",
				"Mac",
				"Phones",
				"iPhone",	
				"Android", 	
				"Security",
				"All");
		})
		.expectComplete()
		.verify();	
	}
	
	@Test
	public void testRemoveCategory2() {
		Mono<Void> removedCategory = service.removeCategory("Computers");
		
		StepVerifier.create(removedCategory)
		.expectComplete()
		.verify();
		
		Flux<Category> all = categoryRepository.findAll();
		
		StepVerifier.create(all)
		.recordWith(ArrayList::new)
		.expectNextCount(5)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(5);		
			assertThat(results)
			.extracting(Category::getName)
			.contains(
				"Phones",
				"iPhone",	
				"Android", 	
				"Security",
				"All");
		})
		.expectComplete()
		.verify();	
		
	}	
	
	@Test
	public void testRemoveCategory3() {
		Mono<Void> removedCategory = service.removeCategory("Security");
		
		StepVerifier.create(removedCategory)
		.expectComplete()
		.verify();
		
		Flux<Category> all = categoryRepository.findAll();
		
		StepVerifier.create(all)
		.recordWith(ArrayList::new)
		.expectNextCount(1)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(1);		
			assertThat(results)
				.extracting(Category::getName)
				.contains(
						"All"
				);
		})
		.expectComplete()
		.verify();	
		
	}	
	
	@Test
	public void testGetAncestorsIdsSingle() {				
		String categoryMac = "Mac";
		Flux<String> ancestors = service.getAncestorsIdsExcluded(new HashSet<>(Arrays.asList(categoryMac)));
		
		StepVerifier.create(ancestors)
		.recordWith(ArrayList<String>::new)
		.expectNextCount(3)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(3);
			assertThat(results)
			.containsExactlyInAnyOrder(
				"All", "Security", "Computers");
		})
		.expectComplete()
		.verify();		
	}
	
	@Test
	public void testGetAncestorsIdsMultiple() {				
		String categoryMac = "Mac";
		String categoryIPhone = "iPhone";	
		Flux<String> ancestors = service.getAncestorsIdsExcluded(new HashSet<>(Arrays.asList(categoryMac, categoryIPhone)));  
		
		StepVerifier.create(ancestors)
		.recordWith(ArrayList<String>::new)
		.expectNextCount(4)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(4);
			assertThat(results)
			.containsExactlyInAnyOrder(
				"All", "Security", "Computers", "Phones");
		})
		.expectComplete()
		.verify();			
	}		
	
	@Test
	public void testGetAncestorsIdsMultipleWithExcessParents() {				
		String categoryMac = "Mac";
		String categoryIPhone = "iPhone";
		String categorySecurity = "Security";
		String categoryPhones = "Phones";
		Flux<String> ancestors = service.getAncestorsIdsExcluded(
				new HashSet<>(Arrays.asList(categoryMac, categoryIPhone, categorySecurity, categoryPhones)));
		
		StepVerifier.create(ancestors)
		.recordWith(ArrayList<String>::new)
		.expectNextCount(4)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(4);
			assertThat(results)
			.containsExactlyInAnyOrder(
				"All", "Security", "Computers", "Phones");
		})
		.expectComplete()
		.verify();				
	}		
	
	@Test
	public void testGetAncestorsMultiple() {
		String categoryIPhone = "iPhone";
		String categoryMac = "Mac";
		Flux<Category> ancestors = service.getAncestorsIncluded(new HashSet<>(Arrays.asList(categoryIPhone, categoryMac)));
		
		StepVerifier.create(ancestors)
		.recordWith(ArrayList::new)
		.expectNextCount(6)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(6);
			assertThat(results)
			.extracting(Category::getName)
			.containsExactlyInAnyOrder(
				"All", "Security", "Computers", "Phones", "iPhone", "Mac");
		})
		.expectComplete()
		.verify();		
	}		
	
	@Test
	public void testGetAncestorsMultipleWithExcessParents() {
		String categoryIPhone = "iPhone";
		String categoryMac = "Mac";
		String categorySecurity = "Security";
		String categoryPhones = "Phones";		
		Flux<Category> ancestors = service.getAncestorsIncluded(
				new HashSet<>(Arrays.asList(categoryMac, categoryIPhone, categorySecurity, categoryPhones)));
		
		StepVerifier.create(ancestors)
		.recordWith(ArrayList::new)
		.expectNextCount(6)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(6);
			assertThat(results)
			.extracting(Category::getName)
			.containsExactlyInAnyOrder(
				"All", "Security", "Computers", "Phones", "iPhone", "Mac");
		})
		.expectComplete()
		.verify();		
	}		
	
	@Test
	public void testGetAncestorsExcludingCategoriesMultiple() {
		String categoryIPhone = "iPhone";
		String categoryMac = "Mac";
		Flux<Category> ancestors = service.getAncestorsExcluded(new HashSet<>(Arrays.asList(categoryIPhone, categoryMac)));
		
		StepVerifier.create(ancestors)
		.recordWith(ArrayList::new)
		.expectNextCount(4)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(4);
			assertThat(results)
			.extracting(Category::getName)
			.containsExactlyInAnyOrder(
				"All", "Security", "Computers", "Phones");			
		})
		.expectComplete()
		.verify();		
	}	
	
	@Test
	public void testGetAncestorsExcludingCategoriesMultipleWithExcessParents() {
		String categoryIPhone = "iPhone";
		String categoryMac = "Mac";
		String categorySecurity = "Security";
		String categoryPhones = "Phones";				
		Flux<Category> ancestors = service.getAncestorsExcluded(
				new HashSet<>(Arrays.asList(categoryIPhone, categoryMac, categorySecurity, categoryPhones)));
		
		StepVerifier.create(ancestors)
		.recordWith(ArrayList::new)
		.expectNextCount(4)
		.consumeRecordedWith(results -> {
			assertThat(results).hasSize(4);
			assertThat(results)
			.extracting(Category::getName)
			.containsExactlyInAnyOrder(
				"All", "Security", "Computers", "Phones");
		})
		.expectComplete()
		.verify();
	}
		
	@Test
	public void testGetDescendantsCount() {
		String categoryIPhone = "iPhone";
		String categoryPhones = "Phones";
		String categoryAll = "All";
				
		Mono<Long> countPhones = service.getDescendantsCount(categoryPhones);				
		StepVerifier.create(countPhones)
		.expectNext(Long.valueOf(2l))
		.expectComplete()
		.verify();
		
		Mono<Long> countIPhone = service.getDescendantsCount(categoryIPhone);		
		StepVerifier.create(countIPhone)
		.expectNext(Long.valueOf(0l))
		.expectComplete()
		.verify();

		Mono<Long> countAll = service.getDescendantsCount(categoryAll);		
		StepVerifier.create(countAll)
		.expectNext(Long.valueOf(6l))
		.expectComplete()
		.verify();
	}	
	
	@Test
	public void testGetTree() {		
		Mono<List<CategoryDTO>> tree = service.getTree("All");
		
		StepVerifier.create(tree)	
			.recordWith(ArrayList::new)
			.expectNextCount(1)
			.consumeRecordedWith(results -> {
				// Check list size
				assertThat(results).hasSize(1);
				// Check root of tree
				CategoryDTO dto = results.iterator().next().get(0);
				assertThat(dto.getName()).isEqualTo("All");
				assertThat(dto.getParent()).isEqualTo(null);
				// Check DTO tree structure
				CategoryDTO security = dto.getChildren().get(0);
				assertTrue(security.getName().equals("Security"), 
						"Expected Security but actual is " + dto.getChildren().get(0).getName());
				assertTrue(security.getChildren().get(0).getName().equals("Computers"), 
						"Expected Computers but actual is " + security.getChildren().get(0).getName());
				assertTrue(security.getChildren().get(1).getName().equals("Phones"), 
						"Expected Phones but actual is " + security.getChildren().get(1).getName());
				assertTrue(security.getChildren().get(1).getChildren().get(0).getName().equals("Android"), 
						"Expected Android but actual is " + security.getChildren().get(1).getChildren().get(0).getName());
				assertTrue(security.getChildren().get(1).getChildren().get(1).getName().equals("iPhone"), 
						"Expected iPhone but actual is " + security.getChildren().get(1).getChildren().get(1).getName());
				
			})
			.expectComplete()
			.verify();
	}	
}

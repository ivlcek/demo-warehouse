package com.warehouse.demo.service;

import static org.assertj.core.api.Assertions.assertThat;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Sort.Direction;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.test.context.junit4.SpringRunner;

import com.warehouse.demo.model.Category;
import com.warehouse.demo.model.Product;

import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import reactor.test.StepVerifier;

@RunWith(SpringRunner.class)
@SpringBootTest
public class ProductServiceTests {
	
	@Autowired
	private ProductService service;
		
	@Autowired
	private MongoOperations mongoOperations;

	@Before
	public void setUp() {	
		mongoOperations.dropCollection(Product.class);
		mongoOperations.dropCollection(Category.class);
		
		mongoOperations.insert(new Category("Android", "Phones", (Arrays.asList("All", "Security", "Phones"))));
		mongoOperations.insert(new Category("iPhone", "Phones", (Arrays.asList("All", "Security", "Phones"))));
		mongoOperations.insert(new Category("Phones", "Security", (Arrays.asList("All", "Security"))));
		mongoOperations.insert(new Category("Mac", "Computers", (Arrays.asList("All", "Security", "Computers"))));
		mongoOperations.insert(new Category("Computers", "Security", (Arrays.asList("All", "Security"))));
		mongoOperations.insert(new Category("Security","All", (Arrays.asList("All"))));
		mongoOperations.insert(new Category("All", null, new ArrayList<>()));
		mongoOperations.findAll(Category.class).forEach(System.out::println);
		
		mongoOperations.insert(new Product("1", "Free Antivirus", "Free Antivirus for Mac computers", BigDecimal.ZERO, Arrays.asList("Mac")));
		mongoOperations.insert(new Product("2", "Premium Phone Security", "Premium Security product for all Mobile Phones", BigDecimal.TEN, Arrays.asList("Phones")));
		mongoOperations.insert(new Product("3", "Ultimate", "Ultimate security product for all devices", BigDecimal.TEN, Arrays.asList("Security")));
		mongoOperations.findAll(Product.class).forEach(System.out::println);		
	}
	
	@After
	public void cleanUp() {
		mongoOperations.dropCollection(Product.class);
		mongoOperations.dropCollection(Category.class);
	}	
	
	@Test
	public void testFindAll() {
		Flux<Product> products = service.findAll();
		StepVerifier.create(products.collectList()).expectNextMatches(foundProducts -> {
			assertThat(foundProducts).hasSize(3);
			assertThat(foundProducts).extracting("id").containsExactlyInAnyOrder("1", "2", "3");
			return true;
			})
		.expectComplete()
		.verify();
	}
	
	@Test
	public void testFindByExamplePageableEmptyProduct() {				
		Product product = new Product();								
		Flux<Product> products = service.findByExamplePageable(product, 0, 10, "id", Direction.ASC);
		StepVerifier.create(products.collectList()).expectNextMatches(foundProducts -> {
			assertThat(foundProducts).hasSize(3);
			assertThat(foundProducts).extracting("id").containsExactlyInAnyOrder("1", "2", "3");
			return true;
		})
		.expectComplete()
		.verify();
	}
	
	@Test
	public void testFindByExamplePageableName() {
		Product product = new Product();
		product.setName("tivirus");
		
		Flux<Product> products = service.findByExamplePageable(product, 0, 10, "id", Direction.ASC);
		StepVerifier.create(products.collectList()).expectNextMatches(foundProducts -> {
			assertThat(foundProducts).hasSize(1);
			assertThat(foundProducts).extracting("id").containsExactly("1");
			return true;
		})
		.expectComplete()
		.verify();	
	}
	
	@Test
	public void testFindByExamplePageableFreeProduct() {
		Product product = new Product();
		product.setPrice(BigDecimal.ZERO);
		
		Flux<Product> products = service.findByExamplePageable(product, 0, 10, "id", Direction.ASC);
		StepVerifier.create(products.collectList()).expectNextMatches(foundProducts -> {
			assertThat(foundProducts).hasSize(1);
			assertThat(foundProducts).extracting("id").containsExactly("1");
			return true;
		})
		.expectComplete()
		.verify();	
	}	
	
	@Test
	public void testFindByExamplePageableDescription() {
		Product product = new Product();
		product.setDescription("product");
		
		Flux<Product> products = service.findByExamplePageable(product, 0, 10, "id", Direction.ASC);
		StepVerifier.create(products.collectList()).expectNextMatches(foundProducts -> {
			assertThat(foundProducts).hasSize(2);
			assertThat(foundProducts).extracting("id").containsExactlyInAnyOrder("2", "3");
			return true;
		})
		.expectComplete()
		.verify();	
	}		
	
	@Test
	public void testFindByExamplePageableCategorySecurity() {
		Product product = new Product();
		product.setCategories(Arrays.asList("Security"));
		
		Flux<Product> products = service.findByExamplePageable(product, 0, 10, "id", Direction.ASC);
		StepVerifier.create(products.collectList()).expectNextMatches(foundProducts -> {
			assertThat(foundProducts).hasSize(3);
			assertThat(foundProducts).extracting("id").containsExactly("1", "2", "3");
			return true;
		})
		.expectComplete()
		.verify();	
	}				

	@Test
	public void testFindByExamplePageableCategoryPhones() {
		Product product = new Product();
		product.setCategories(Arrays.asList("Phones"));
		
		Flux<Product> products = service.findByExamplePageable(product, 0, 10, "id", Direction.ASC);
		StepVerifier.create(products.collectList()).expectNextMatches(foundProducts -> {
			assertThat(foundProducts).hasSize(2);
			assertThat(foundProducts).extracting("id").containsExactly("2", "3");
			return true;
		})
		.expectComplete()
		.verify();	
	}				
	
	@Test
	public void testFindByExamplePageableCombination1() {
		Product product = new Product();
		product.setName("m"); 					// -> [Ultimate, Premium Phone Security]
		product.setDescription("product");		// -> [Ultimate, Premium Phone Security]
		product.setCategories(Arrays.asList("Phones")); // -> [Ultimate, Premium Phone Security]
		
		Flux<Product> products = service.findByExamplePageable(product, 0, 10, "id", Direction.ASC);
		StepVerifier.create(products.collectList()).expectNextMatches(foundProducts -> {
			assertThat(foundProducts).hasSize(2);
			assertThat(foundProducts).extracting("id").containsExactly("2", "3");
			return true;
		})
		.expectComplete()
		.verify();
	}
	
	@Test
	public void testFindByExamplePageableCombination2() {
		Product product = new Product();
		product.setName("ma"); 					// -> [Ultimate]
		product.setDescription("product");		// -> [Ultimate, Premium Phone Security]
		product.setCategories(Arrays.asList("Phones")); // -> [Ultimate, Premium Phone Security]
		
		Flux<Product> products = service.findByExamplePageable(product, 0, 10, "id", Direction.ASC);
		StepVerifier.create(products.collectList()).expectNextMatches(foundProducts -> {
			assertThat(foundProducts).hasSize(1);
			assertThat(foundProducts).extracting("id").containsExactly("3");
			return true;
		})
		.expectComplete()
		.verify();
	}	
	
	@Test
	public void testSave() {
		Product product = new Product(null, "Spyware Protection Mac", "Free Spyware for Mac computers", BigDecimal.ZERO, Arrays.asList("Mac"));
		Mono<Product> storedProduct = service.save(product);
		
		StepVerifier.create(storedProduct)
		.expectNextMatches(p -> {
			assertThat(p.getId()).isNotNull();
			assertThat(p.getName()).isEqualTo("Spyware Protection Mac");
			assertThat(p.getDescription()).isEqualTo("Free Spyware for Mac computers");
			assertThat(p.getPrice()).isEqualTo(BigDecimal.ZERO);
			assertThat(p.getCategories()).contains("Mac");
			return true;
		})
		.expectComplete()
		.verify();
	}	
	
	@Test
	public void testDelete() {
		Flux<Product> deleted = service.deleteByIds(Arrays.asList("1", "2"))
				.thenMany(service.findAll());
		
		StepVerifier.create(deleted.collectList())
			.expectNextMatches(foundProducts -> {
				assertThat(foundProducts).hasSize(1);
				assertThat(foundProducts).extracting("id").containsExactlyInAnyOrder("3");
				return true;
			})
			.expectComplete()
			.verify();
	}	
	
	@Test
	public void testDeleteAll() {
		Flux<Product> deleted = service.deleteByIds(Arrays.asList("1", "2", "3"))
				.thenMany(service.findAll());
		
		StepVerifier.create(deleted.collectList())
			.expectNextMatches(foundProducts -> {
				assertThat(foundProducts).hasSize(0);
				return true;
			})
			.expectComplete()
			.verify();
	}		
	
	@Test
	public void testUpdate() {
		Product product = new Product("1", "Spyware Protection Mac", "Free Spyware for Mac computers", BigDecimal.ONE, Arrays.asList("Computers"));
		Mono<Product> storedProduct = service.save(product);
		
		StepVerifier.create(storedProduct)
		.expectNextMatches(p -> {
			assertThat(p.getId()).isEqualTo("1");
			assertThat(p.getName()).isEqualTo("Spyware Protection Mac");
			assertThat(p.getDescription()).isEqualTo("Free Spyware for Mac computers");
			assertThat(p.getPrice()).isEqualTo(BigDecimal.ONE);
			assertThat(p.getCategories()).contains("Computers");
			return true;
		})
		.expectComplete()
		.verify();
	}
}
